import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client
{
    private static boolean ackValue;
	private static boolean ackSent;
	private static final int ACKSIZE = 13;	//Size of the ACK buffer
	public static void main( String[] args ) throws IOException
	{
		ackValue = false;
		ackSent = false;
		testArgs( args.length );
		String server = args[ 0 ];       // Server name/IP	
		int servPort = ( args.length == 2 ) ? Integer.parseInt(args[1]) : 7;
		
		//set up
		byte[] RequestData = "Client Requesting Service".getBytes();
		
		System.out.println("Will now attempt to connect to server at " + server + ", Port "  + servPort  );
		
		
		//Connect to the server
		Socket socket = new Socket(server, servPort);
		System.out.println("Connected to server");
		

		int count = 0;
		while(count < 10){
			try {
				Thread.sleep(3000);
			} catch (Exception ex) {}
			//wait till confirmation received from server then do handle session for encoding
			if(ackValue){
				handleSession( socket );
			}
			else if(!ackSent){
				//Begin handshake
					handShake ( socket, RequestData );
					ackSent = true;
				}
			count++;
			if(count == 10){
			//user idleness, depends on how it should be implemented
				System.out.println("\nTime taken too long for server to respond, sending another handshake");
				count = 0;
				ackSent = false;
			}
		}
		socket.close();
		
	}
	static void testArgs( int numArgs ) throws IOException
	{
		if ( numArgs < 1 || numArgs > 2 )
			throw new IllegalArgumentException( "Required parameter(s): ServerName/IP (Port#)" );
	}
	
	static void handShake( Socket serverConn, byte[] request ) throws IOException
	{
		String acknowledgement = "Ready To Help";
		//server response to receive
		byte[] response = new byte[ACKSIZE];
		//Expect Ready to help string response from server
	
		//System.out.println("Attempting Handshake");
		InputStream in = serverConn.getInputStream();
		OutputStream out = serverConn.getOutputStream();
		
		//Send confirmation data to server
		out.write(request);
		
		//wait for confirmation from Server
		int totalBytesRcvd = 0;
		int bytesRcvd;
		
		in.read( response );
		String responseString = new String(response);
		if(responseString.equals(acknowledgement)){
			ackValue = true;
		}
		/*
		while(totalBytesRcvd < acknowledgement.length){
			if((bytesRcvd = in.read(response, totalBytesRcvd, response.length - totalBytesRcvd)) == -1)
				throw new SocketException("Connection closed prematurely");
                        
			totalBytesRcvd += bytesRcvd;
		}*/
		
	}
	
	
	static void handleSession( Socket serverConn  ) throws IOException
	{
		System.out.println("Works, Continue messaging from here");
		serverConn.close();
		/*
		InputStream in = serverConn.getInputStream();
		OutputStream out = serverConn.getOutputStream();
		
		

		byte[] byteBuffer = "basic I/O".getBytes(); // REPLACE
		out.write( byteBuffer );

		while ( in.read( byteBuffer ) != -1 )
			System.out.println( "Received: " + new String( byteBuffer ) ); // REPLACE
			*/
	}
	
	/*		( potential algorithm )
	 * open socket
	 * if ( don't receive idle )
	 *  	 sleep 1 sec
	 * else
	 * 		write startAsk
	 * 		if ( don't receive ack )
	 * 			sleep 1 sec, return
	 * 		else
	 * 			write message
	 * 			write end
	 * 	another message?
	 * close connection
	 */
}
