import java.net.*;
import java.io.*;

public class Server
{
	private static final int BUFSIZE = 32;   // Size of receive buffer
	private static final int ACKSIZE = 25;	//Size of the ACK buffer
	private static byte[] byteBuffer;
	private static boolean ackValue;
	
	public static void main( String[] args ) throws IOException
	{
		testArgs( args.length );	
		int servPort = Integer.parseInt(args[0]);
		ServerSocket servSock = new ServerSocket(servPort);
		byteBuffer = new byte[ BUFSIZE ];
		
		ackValue = false;//initial setting for the hand
		
		
		System.out.println("Server is now ready to receive client at port " + servPort);
		//Setup of the sockets, in, and out
		Socket clntSock = servSock.accept();
		SocketAddress clientAddress = clntSock.getRemoteSocketAddress();
		while( true ){
			//Setup of the sockets, in, and out
			//Socket clntSock = servSock.accept();
			//SocketAddress clientAddress = clntSock.getRemoteSocketAddress();
			System.out.println("Handling client at " + clientAddress);

			if(ackValue)
			{
				handleClients( servSock, clntSock );
				break;
				}
			else
			
				handShake( servSock, clntSock );
          }
		servSock.close();
     }
	
	static void testArgs( int numArgs ) throws IOException
	{
		if ( numArgs != 1 )
			throw new IllegalArgumentException( "Only parameter: Port#" );
	}
	
	static void handShake( ServerSocket servSock, Socket clntSock) throws IOException
	{
		System.out.println("Checking for handshake");
		String expectedHand = "Client Requesting Service";
		byte[] acknowledgement = "Ready To Help".getBytes(); 
		byte[] receiveHand = new byte[ACKSIZE];

		InputStream in = clntSock.getInputStream();
		OutputStream out = clntSock.getOutputStream();
		
		while ( true )
		{
			in.read( receiveHand );
			String received = new String(receiveHand);
			String ack = new String(acknowledgement);
			System.out.println(ack.length());
			if(received.equals(expectedHand))
			{
				out.write( acknowledgement ); // echoing action // REPLACE
				ackValue = true;
				break;
			}
			else{
				System.out.println("Client request string rejected");
			}
		}
		
		System.out.println("Received: " + new String(receiveHand));
	}
	
	static void handleClients( ServerSocket servSock,  Socket clnt ) throws IOException
	{
		System.out.println("Continue Messaging from here");
		servSock.close();
		//InputStream in = clntSock.getInputStream();
		//OutputStream out = clntSock.getOutputStream();
		
		//Encoding work will continue here.
		/*
		while ( in.read( byteBuffer ) != -1 )
		{
			for ( byte nn : byteBuffer )
				System.out.print( (char) nn ); // printing to screen // REPLACE
			out.write( byteBuffer ); // echoing action // REPLACE
		}
                clnt.close();
				*/
		
	}
	
	/*		( potential algorithm )
	 *  socket.accept connection
	 *  	[ assuming server 'reports' state rather than listening by default ]
	 *  decide state
	 *  	quiet : sleep 1 sec, return
	 *  	idle : read
	 *  	- if ( received ask )
	 *  	- decide connection state
	 *  		quiet : sleep 1 sec, return
	 *  		acknowledge : read message; (echo? react? print?)
	 *  		if ( received end )
	 *  		- close connection
	 */
}
