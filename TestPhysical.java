
public class TestPhysical
{

	public static void main( String[] args )
	{
		PhysicalLayer link = new PhysicalLayer( );
		
		System.out.println( "\tTesting output" );
		String bits = testOutput( link );

		System.out.println( "\tTesting input" );
		pretestInput( link );
		testInput( link, bits );
	}
	
	static String testOutput( PhysicalLayer link )
	{
		String output;
		String message = "/>M\\kz!0HY";
		System.out.println( "Testing />M\\kz!0HY" );
		System.out.println( "in hex: 2F 3E 4D 5C 6B 7A 21 30 48 59" );
		output = link.encode( message );
		System.out.printf( "Encoded bits: %s\n", output );
		// Produces codes matching 2F 3E 4D 5C 6B 7A 31 30 48 59
		return output;
	}
	
	static void pretestInput( PhysicalLayer link )
	{
		System.out.println( "Should print quiet, ack, end, idle, null, ask" );
		String[ ] inputs = { "00000", "10001", "10011", "11111", "11010", "11000" };
		for ( String received : inputs )
			System.out.print( reportCommInterpreted( link, received ) );
	}
	
	static String reportCommInterpreted( PhysicalLayer link, String toTest )
	{
		switch( link.whichSpecialInput( toTest ) )
		{
		case quiet:
			return "\tquiet";
		case idle:
			return "\tidle";
		case startAsk:
			return "\task";
		case startAck:
			return "\tack";
		case end:
			return "\tend";
		case NULL:
			return "\tnull";
		default:
			return "\tnull";
		}
	}
	
	static void testInput( PhysicalLayer link, String inputBits )
	{
		String decBits=link.fiveB4B( inputBits );
		System.out.printf( "Decoded bits: %s\n", decBits );
		String text=link.decode(decBits);
		System.out.printf( "Text Decoded bits: %s\n", text );
	}
	/*0	( "11110" ) 	1	( "01001" ) 	2	( "10100" ) 	3	( "10101" )
	4	( "01010" ) 	5	( "01011" ) 	6	( "01110" ) 	7	( "01111" )
	8	( "10010" ) 	9	( "10011" ) 	A	( "10110" ) 	B	( "10111" )
	C	( "11010" ) 	D	( "11011" ) 	E	( "11100" ) 	F	( "11101" )*/
}
